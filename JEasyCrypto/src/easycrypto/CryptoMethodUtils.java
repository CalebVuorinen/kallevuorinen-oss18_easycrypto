package easycrypto;

abstract class CryptoMethodUtils implements CryptoMethod {

	public static boolean isValidString(String testedString) {
		return (testedString != null && !testedString.isEmpty());
	}
}
