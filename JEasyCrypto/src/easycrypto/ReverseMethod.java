package easycrypto;

import easycrypto.EasyCryptoAPI.Result;
import easycrypto.EasyCryptoAPI.ResultCode;

class ReverseMethod implements CryptoMethod {

	@Override
	public Result encrypt(final String toEncrypt) {
		Result result;
		
		if (CryptoMethodUtils.isValidString(toEncrypt)) {
			String toStoreTo = new StringBuilder(toEncrypt).reverse().toString();
			result = new Result(ResultCode.ESuccess, toStoreTo);
		}
		else {
			result = new Result(ResultCode.EError, "Empty string");
		}
		
		return result;
	 	}
	
	@Override
	public Result decrypt(final String toDecrypt) {
		return encrypt(toDecrypt);
	}

	@Override
	public String method() {
		return "reverse";
	}

}
